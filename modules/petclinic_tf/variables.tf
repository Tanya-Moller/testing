# Defining variables

variable "region" {
  description = "AWS Deployment region.."
  default = "eu-west-1"
}

variable "environment_tag" {
    description = "this is the common environment tag"
    default = "bt"
}

variable "public_key_path" {
    description = "This is the path to the ssh key"
    default = "~/.ssh/authorized_keys"
}

variable "db_name" {
    description = "Name of the RDS DB"
    default = "petclinic_db"
}

variable "db_user" {
    description = "Username of the RDS DB"
    default = "admin"
}

variable "db_password" {
    description = "Password of the RDS DB"
    default = "secret!"
}

variable "vpc_id" {

}

variable "private_subnet_id_1a" {

}

variable "private_subnet_id_1b" {

}

variable "private_subnet_id_1c" {

}

variable "sg_web_id" {

}

variable "sg_jenkins_master_id" {

}

variable "sg_jenkins_worker_id" {

}
