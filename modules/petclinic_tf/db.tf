# Building DB for PetClinic
## Database SG
resource "aws_security_group" "sg_db" {
  name = "sg_${var.environment_tag}_database"
  vpc_id = var.vpc_id
  description = "Allow 3306 from webservers and SSH from Jenkins"
  ingress {
      from_port   = 0
      to_port     = 3306
      protocol    = "tcp"
      security_groups = [var.sg_web_id]
  }
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = [var.sg_jenkins_master_id, var.sg_jenkins_worker_id]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-database"
  }
}

resource "aws_db_subnet_group" "default" {
  name = "${var.environment_tag}-db-subnet-group"
  subnet_ids = [var.private_subnet_id_1a, var.private_subnet_id_1b, var.private_subnet_id_1c] # Need to add second private subnet here
}

resource "aws_db_instance" "default" {
  allocated_storage = 20
  storage_type = "gp2"
  multi_az = true
  engine = "mysql"
  engine_version = "5.7.16"
  instance_class = "db.t2.micro"
  name = "${var.db_name}"
  username = "${var.db_user}"
  password = "${var.db_password}"
  skip_final_snapshot = true
  vpc_security_group_ids = ["${aws_security_group.sg_db.id}"]
  db_subnet_group_name = "${aws_db_subnet_group.default.name}"
  identifier = "${var.environment_tag}-db"

  tags = {
      Name = "${var.environment_tag}_DB"
  }
}
