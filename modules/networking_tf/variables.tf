# Defining variables

variable "region" {
  description = "AWS Deployment region.."
  default = "eu-west-1"
}

variable "environment_tag" {
    description = "this is the common environment tag"
    default = "bt"
}

variable "public_key_path" {
    description = "This is the path to the ssh key"
    default = "~/.ssh/authorized_keys"
}