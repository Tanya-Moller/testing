# Setting up VPC & Networking

## Setting up a VPC

resource "aws_vpc" "vpc" {
  cidr_block = "90.123.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    "Name" = "bt-vpc"
    "Environment" = "${var.environment_tag}"
  }
}

# Setting up an IGW
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Name" = "bt-igw"
  }
}

# Setting up subnets and route tables
resource "aws_subnet" "subnet_public" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1a"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Public"
    "Name" = "${var.environment_tag}-public-subnet"
  }
}
resource "aws_route_table" "rtb_public" {
  vpc_id = "${aws_vpc.vpc.id}"
route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.igw.id}"
  }
tags = {
    "Name"        = "${var.environment_tag}-public-route-table"
    "Environment" = "${var.environment_tag}"
  }
}
resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = "${aws_subnet.subnet_public.id}"
  route_table_id = "${aws_route_table.rtb_public.id}"
}

resource "aws_subnet" "subnet_private_1a" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.0.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1a"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Private"
    "Name" = "${var.environment_tag}-private-subnet-1a"
  }
}

resource "aws_subnet" "subnet_private_1b" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.2.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1b"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Private"
    "Name" = "${var.environment_tag}-private-subnet-1b"
  }
}

resource "aws_subnet" "subnet_private_1c" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "90.123.4.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1c"
  tags = {
    "Environment" = "${var.environment_tag}"
    "Type" = "Private"
    "Name" = "${var.environment_tag}-private-subnet-1c"
  }
}

resource "aws_route_table" "rtb_private" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    "Name"        = "${var.environment_tag}-private-route-table"
    "Environment" = "${var.environment_tag}"
  }
}

resource "aws_route_table_association" "rta_subnet_private_1a" {
  subnet_id      = "${aws_subnet.subnet_private_1a.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}

resource "aws_route_table_association" "rta_subnet_private_1b" {
  subnet_id      = "${aws_subnet.subnet_private_1b.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}

resource "aws_route_table_association" "rta_subnet_private_1c" {
  subnet_id      = "${aws_subnet.subnet_private_1c.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}

resource "aws_main_route_table_association" "rta_private" {
  vpc_id         = "${aws_vpc.vpc.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}

# Creating NACLs

resource "aws_network_acl" "public-nacl" {
  vpc_id = "${aws_vpc.vpc.id}"
  subnet_ids = [ "${aws_subnet.subnet_public.id}" ]
  egress {
    protocol   = -1
    rule_no    = 101
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    icmp_code  = 0
    icmp_type  = 0
    from_port  = 0
    to_port    = 0
  }
  ingress {
    protocol   = -1
    rule_no    = 101
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    icmp_code  = 0
    icmp_type  = 0
    from_port  = 0
    to_port    = 0
  }
  tags = {
    "Name" = "${var.environment_tag}-public-nacl"
  }
}

resource "aws_default_network_acl" "default" {
  default_network_acl_id = "${aws_vpc.vpc.default_network_acl_id}"
  subnet_ids = [ "${aws_subnet.subnet_private_1a.id}", "${aws_subnet.subnet_private_1b.id}" , "${aws_subnet.subnet_private_1c.id}" ]
  lifecycle {
    ignore_changes = [subnet_ids]
  }
  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "${aws_vpc.vpc.cidr_block}"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  tags = {
    "Name"        = "${var.environment_tag}-private-nacl"
  }
}

# Creating Security Groups

## Jenkins SG
resource "aws_security_group" "sg_jenkins_master" {
  name = "sg_${var.environment_tag}_jenkins_master"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow access from home IPs"
  ingress {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["90.212.182.216/32", "82.33.58.195/32"]
  }
#   ingress {
#       from_port   = 0
#       to_port     = 0
#       protocol    = "-1"
#       cidr_blocks = ["90.123.0.1/32"] #Jenkins worker private IPs need to go here
#   }
# ingress {
#       from_port   = 22
#       to_port     = 22
#       protocol    = "tcp"
#       cidr_blocks = ["${aws_instance.jenkins_master.ip}"] #Jenkins worker & master public IPs need to go here
#   }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-jenkins_master"
  }
}

resource "aws_security_group" "sg_jenkins_worker" {
  name = "sg_${var.environment_tag}_jenkins_worker"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow access from home IPs and Jenkins IPs"
  ingress {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["90.212.182.216/32", "82.33.58.195/32"]
  }
  ingress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      security_groups = [aws_security_group.sg_jenkins_master.id] #### Worker needs master IP
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-jenkins_worker"
  }
}

## Loadbalancer SG
resource "aws_security_group" "sg_lb" {
  name = "sg_${var.environment_tag}_loadbalancer"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow access from all and SSH from Jenkins and bastion"
  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = [aws_security_group.sg_jenkins_master.id, aws_security_group.sg_jenkins_worker.id, aws_security_group.sg_bastion.id]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-loadbalancer"
  }
}

## Webserver SG
resource "aws_security_group" "sg_web" {
  name = "sg_${var.environment_tag}_webserver"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow 8080 from lb and SSH from Jenkins and bastion"
  ingress {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      security_groups = [aws_security_group.sg_lb.id]
  }
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = [aws_security_group.sg_jenkins_master.id, aws_security_group.sg_jenkins_worker.id, aws_security_group.sg_bastion.id]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-webserver"
  }
}

## Bastion SG
resource "aws_security_group" "sg_bastion" {
  name = "sg_${var.environment_tag}_bastion"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow access from home IPs"
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["90.212.182.216/32", "82.33.58.195/32"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-bastion"
  }
}



# Setting up key pair path

resource "aws_key_pair" "ec2key" {
  key_name = "BTKEY"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgtW6mOA9ykq+cq5ovOtMQqVVavRp7WlQr27PQpcg7Uvm/cXB0yk8fPoh+5Nl0R9FN4QPwPmD3f/gb6xDMXIJ6w94z4VnP6hLWJdi8hc5wwSR+Oskjxu2EfzzQI64kzS+jUFSl81Rqdv7G8o/Vooo3s0G0x8cKdGsbocLXIKA338yxnic/Zz/HBVqTe/u1BYeNWUeR1/RS5qs5IYXzBX7Q3SyqjMaBBRWRQVhWWvG7WnIXmtM0ID5VVhBFt2G1EV4orgnJ+OdZyA1buYgUshrfs/TzlSwLjuWImOCnieqh/qYD6a0BxnhC7Wyy31bibfiDlxivP/579dUfVId36TVh BTKEY"
}

