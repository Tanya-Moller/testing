resource "aws_instance" "jenkins_master" {
  ami = "ami-0224148196d608df4"
  instance_type = "t2.micro"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
  subnet_id = aws_subnet.subnet_public.id
  security_groups = [aws_security_group.sg_jenkins_master.id]
  key_name = "BTKEY"
  tags = {
      "Name" = "bt-jenkins-master"
  }
}

resource "aws_instance" "jenkins_worker" {
  ami = "ami-06452ff11523e2f53"
  count = 2
  instance_type = "t2.micro"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
  subnet_id = aws_subnet.subnet_public.id
  security_groups = [aws_security_group.sg_jenkins_worker.id]
  key_name = "BTKEY"
  tags = {
      "Name" = "bt-jenkins-worker-${count.index}"
  }
}

