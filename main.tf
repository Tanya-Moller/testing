module "networking" {
  source = "./modules/networking_tf"
  # Pass some variables
}

module "petclinic" {
  source = "./modules/petclinic_tf"
  # Pass some variables

  vpc_id = module.networking.vpc_id
  sg_web_id = module.networking.sg_web_id
  sg_jenkins_master_id = module.networking.sg_jenkins_master_id
  sg_jenkins_worker_id = module.networking.sg_jenkins_worker_id
  private_subnet_id_1a = module.networking.private_subnet_id_1a
  private_subnet_id_1b = module.networking.private_subnet_id_1b
  private_subnet_id_1c = module.networking.private_subnet_id_1c
}

# Setting up key pair path

resource "aws_key_pair" "ec2key" {
  key_name = "BTKEY"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgtW6mOA9ykq+cq5ovOtMQqVVavRp7WlQr27PQpcg7Uvm/cXB0yk8fPoh+5Nl0R9FN4QPwPmD3f/gb6xDMXIJ6w94z4VnP6hLWJdi8hc5wwSR+Oskjxu2EfzzQI64kzS+jUFSl81Rqdv7G8o/Vooo3s0G0x8cKdGsbocLXIKA338yxnic/Zz/HBVqTe/u1BYeNWUeR1/RS5qs5IYXzBX7Q3SyqjMaBBRWRQVhWWvG7WnIXmtM0ID5VVhBFt2G1EV4orgnJ+OdZyA1buYgUshrfs/TzlSwLjuWImOCnieqh/qYD6a0BxnhC7Wyy31bibfiDlxivP/579dUfVId36TVh BTKEY"
}
